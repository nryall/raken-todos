import React, { Component } from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

export default class TodoFilter extends Component {
  render() {
    return (
      <FormControlLabel
        control={
          <Checkbox edge="start" checked={false} tabIndex={-1} disableRipple />
        }
        label="Show Completed"
      />
    );
  }
}
