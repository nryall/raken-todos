import React, { Component, Fragment } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Divider from "@material-ui/core/Divider";
import Checkbox from "@material-ui/core/Checkbox";
// import CircularProgress from '@material-ui/core/CircularProgress';

export default class TodoList extends Component {
  render() {
    return (
      <List>
        <Fragment key={1}>
          <ListItem role={undefined} dense button onClick={() => {}}>
            <ListItemIcon>
              <Checkbox
                edge="start"
                checked={false}
                tabIndex={-1}
                disableRipple
              />
            </ListItemIcon>

            <ListItemText id="1" primary={`Todo Item 1`} />

            <ListItemSecondaryAction>
              <IconButton edge="end" aria-label="delete">
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
          <Divider />
        </Fragment>
      </List>
    );
  }
}
