import React, { Component } from "react";
import TodoFilter from "./TodoFilter";
import AddTodo from "./AddTodo";

import Grid from "@material-ui/core/Grid";

//import faker from "faker";
// faker.hacker.phrase()

export default class Footer extends Component {
  render() {
    return (
      <Grid container spacing={6}>
        <Grid item xs={10}>
          <AddTodo />
        </Grid>
        <Grid item xs={2}>
          <TodoFilter />
        </Grid>
      </Grid>
    );
  }
}
