import React, { Fragment, Component } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";

import Container from "@material-ui/core/Container";

import Navbar from "./NavBar";
import TodoList from "./TodoList";
//import Footer from "./Footer";

class App extends Component {
  render() {
    return (
      <Fragment>
        <CssBaseline />
        <Container fixed>
          <Navbar />
          <TodoList />
          {/*<Footer />*/}
        </Container>
      </Fragment>
    );
  }
}

export default App;
