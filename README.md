## Raken Todo Example Application

* A user can manage a list of Todo items where they can create Todos, and mark a Todo as completed.
* A user can delete old Todos
* A Todo Item has the following fields
  * `id`
  * `title`
  * `completed`

## API

- `GET:  /todos`   - Get the authenticated user's list of Todos
- `POST: /todos` - Create a new Todo
- `PATCH: todos/id` - Update a Todo
- `DELETE: todos/id` - Delete a Todo

### Example Todo

```
{
  "id" : "1",
  "title": "Remember to put the baby seat back in the car before driving to daycare.",
  "completed" false
}
```

## Run the Application

In the project directory, you can run:

### `yarn install`

Installs all base the dependencies.

### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `yarn run mock:api`

Launches a mock JSON api. Navigate to [http://localhost:4000/todos](http://localhost:400/todos) on your browser to see the Todos resource.

Any request (GET, POST, PATCH, DELETE etc) you make to the server will be saved automatically to the `api/db.json` file.